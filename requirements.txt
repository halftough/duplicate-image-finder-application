Kivy==2.0.0
Wand==0.6.5
numpy==1.19.4
opencv_python==4.4.0.46
scikit_image==0.18.0
python_magic==0.4.18
