#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import threading
from skimage.metrics import structural_similarity as compare_ssim
from itertools import combinations
from typing import Iterable, List, Optional, Set, Callable, Dict

from imagecomparator.model.exceptions import LoadError
from imagecomparator.model.image import Image
from imagecomparator.model.resultgroup import ResultGroup
from imagecomparator.model.watchable import Watchable

ProgressChangedFunction = Callable[[float], None]
ResultsChangedFunction = Callable[[List[Dict]], None]


class SearchEngine(Watchable):
    class Action(Watchable.Action):
        PROGRESS_CHANGED = 0
        RESULTS_CHANGED = 1

    def __init__(self):
        super().__init__()
        self.include_subdirectories: bool = True
        self.dirs: Optional[List[str]] = None
        self.all_dirs: Set[str] = set()
        self.images: List[Image] = []
        self._progress: float = 0  # Search progress in percents
        self.to_compare: int = 0
        self.search_stop: bool = False
        self.matches: List[ResultGroup] = []
        self.match_map: Dict[str, ResultGroup] = {}

    @property
    def progress(self) -> float:
        return self._progress

    @progress.setter
    def progress(self, value: float):
        self._progress = value
        self.run_handlers(self.Action.PROGRESS_CHANGED, value)

    @property
    def results_data(self) -> List[Dict]:
        """Results data for Kivi"""
        return [{'match': entry} for entry in self.matches]

    def add_match(self, image1: Image, image2: Image):
        group1 = self.match_map.get(image1.path, None)
        group2 = self.match_map.get(image2.path, None)
        if group1 and not group2:
            group1.add_image(image2)
            self.match_map[image2.path] = group1
        elif not group1 and group2:
            group2.add_image(image1)
            self.match_map[image1.path] = group2
        elif not group1 and not group2:
            group = ResultGroup()
            group.on(ResultGroup.Action.IMAGE_REMOVED, self._images_removed)
            group.add_image(image1)
            group.add_image(image2)
            self.matches.append(group)
            self.match_map[image1.path] = group
            self.match_map[image2.path] = group
        # elements are already grouped as both matched one of results
        elif group1 is group2:
            return
        elif group1 and group2:
            group1.absorb(group2)
            self.matches.remove(group2)
            for img in group2:
                self.match_map[img.path] = group1
        self.run_handlers(self.Action.RESULTS_CHANGED, self.results_data)

    def _images_removed(self, group: ResultGroup, images: List[Image]):
        for image in images:
            del self.match_map[image.path]
        if len(group) < 2:
            self.matches.remove(group)

    def _add_directory(self, directory: str):
        for file in os.listdir(directory):
            path = os.path.realpath(os.path.join(directory, file))
            if os.path.isdir(path):
                if self.include_subdirectories and path not in self.all_dirs:
                    self._add_directory(path)
                    self.all_dirs.add(path)
            elif Image.is_image(path):
                self.images.append(Image(path))

    def _search_thread_function(self):
        for directory in self.dirs:
            self._add_directory(directory)
        self.to_compare = len(self.images) * (len(self.images) - 1) / 2
        count: int = 0
        for left, right in combinations(self.images, 2):
            self.compare(left, right)
            count += 1
            self.progress = count / self.to_compare * 100
            if self.search_stop:
                return

    def start_search(self, dirs: Iterable, include_subdirectories: bool):
        self.include_subdirectories = include_subdirectories
        self.dirs = list(dirs)
        thread = threading.Thread(target=self._search_thread_function)
        thread.start()

    def stop_search(self):
        self.search_stop = True

    def compare(self, left: Image, right: Image):
        # TODO; Refactor
        limit_up = 0.5
        limit_down = 0
        if left.skip or right.skip:
            return
        try:
            ssim = compare_ssim(left.ssim_data, right.ssim_data, multichannel=True)
        except LoadError as e:
            e.print_warning()
            return
        if ssim > limit_up or ssim < limit_down:
            self.add_match(left, right)
            # TODO; Sort by quality
