#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Callable


def binary_index_search(element, array, cmp: Callable = lambda a, b: a > b) -> int:
    minimum: int = 0
    maximum: int = len(array)
    if cmp(element, array[-1]):
        return maximum
    while minimum != maximum:
        middle = (minimum + maximum) // 2
        if minimum == middle:
            if cmp(element, array[maximum]):
                return maximum
        else:
            return minimum
        if cmp(element, array[middle]):
            minimum = middle
        else:
            maximum = middle
    return minimum
