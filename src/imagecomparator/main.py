#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


from argparse import ArgumentParser
from typing import Callable, Optional
from kivy import Config
from kivy.app import App
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, SlideTransition

from kivyextras.uix.settings import SettingShortcut
from imagecomparator.assets.assets import Layout, Settings
from imagecomparator.view.filepickerscreen import FilePickerScreen
from imagecomparator.view.mainscreen import MainScreen


class ImageComparatorApp(App):
    class Screens:
        def __init__(self):
            self.file_picker_screen: Optional[FilePickerScreen] = None
            self.main_screen: Optional[MainScreen] = None

    def __init__(self, args, **kwargs):
        self.args = args
        self.screens = self.Screens()
        super().__init__(**kwargs)
        self.screen_manager = ScreenManager()

    def build(self):
        self.title = 'Image Comparator'
        self.screen_manager.transition = SlideTransition(direction='up')
        self.screens.file_picker_screen = FilePickerScreen()
        self.screens.main_screen = MainScreen()
        self.screen_manager.add_widget(self.screens.file_picker_screen)
        self.screen_manager.add_widget(self.screens.main_screen)

        return self.screen_manager

    def build_config(self, config):
        config.setdefaults('info', {
            'font_size': 16,
            'position': 'Top',
            'path': True,
            'path_type': 'Cut shared',
            'size': True,
            'file_size': True,
            'quality': True,
            'attribute_color_best': '4fc7ea',
            'attribute_color_worst': 'ea4f74',
            'zoom_show': True,
            'zoom_position': 'Bottom',
            'zoom_font_size': 16
        })
        config.setdefaults('shortcuts', {
            'toggle_compare_method': 'V',
            'compare_side_by_side': '',
            'compare_one_at_time': '',
            'toggle_zoom': 'F',
            'zoom_fit': '',
            'zoom_100': '',
            'toggle_scaling': 'S',
            'scaling_size': '',
            'scaling_zoom': '',
            'toggle_information': 'I',
            'remove_marked': 'Delete'
        })

    def build_settings(self, settings):
        settings.register_type('shortcut', SettingShortcut)
        with open(Settings.VIEW, mode='r') as file:
            data = file.read()
            settings.add_json_panel('Duplicate Finder', self.config, data=data)
        with open(Settings.SHORTCUTS, mode='r') as file:
            data = file.read()
            settings.add_json_panel('Keyboard Shortcuts', self.config, data=data)

    def on_start(self):
        if self.args.no_subdirs:
            self.screens.file_picker_screen.view.search_subdirectories.active = False
        if self.args.dirs:
            # Hiding transition
            duration = self.screen_manager.transition.duration
            self.screen_manager.transition.duration = 0

            for dir in self.args.dirs:
                self.screens.file_picker_screen.add_path(dir)
            self.screen_manager.current = MainScreen.screen_name
            self.screen_manager.transition.duration = duration

    def on_config_change(self, config, section, key, value):
        for screen in self.screen_manager.screens:
            if hasattr(screen, 'on_config_change') and isinstance(screen.on_config_change, Callable):
                screen.on_config_change()

    def on_stop(self):
        for screen in self.screen_manager.screens:
            if hasattr(screen, 'on_exit') and isinstance(screen.on_exit, Callable):
                screen.on_exit()


def main():
    parser = ArgumentParser()
    parser.add_argument('--dirs', '-d', nargs='+', default=[])
    parser.add_argument('--no-subdirs', '-ns', action='store_true')
    args = parser.parse_args()

    Config.set('input', 'mouse', 'mouse,disable_multitouch')
    Builder.load_file(Layout.MAIN)
    main_window = ImageComparatorApp(args)

    Window.size = (1000, 800)
    main_window.run()
