#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from itertools import combinations
from pathlib import Path
from typing import List, Dict, Generator

from kivy.uix.button import Button
from kivy.uix.checkbox import CheckBox
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen

from kivyextras.uix.selectablerecycleview import SelectableRecycleView, SelectableWidget


class FilePickerScreen(Screen):
    screen_name = 'file_picker_screen'

    class View:
        def __init__(self, ids: Dict):
            self.add_button: Button = ids['add_button']
            self.remove_button: Button = ids['remove_button']
            self.file_picker: FileChooserListView = ids['file_picker']
            self.picked_list: SelectableRecycleView = ids['picked_list']
            self.search_subdirectories: CheckBox = ids['search_subdirectories']

    def __init__(self, **kwargs):
        super().__init__(name=self.screen_name, **kwargs)
        self.view = self.View(self.ids)
        self.added_dirs: List[Dict] = self.view.picked_list.data

    @property
    def picked_dirs_list(self) -> Generator[str, None, None]:
        return (d['text'] for d in self.view.picked_list.data)

    @property
    def added_as_paths(self) -> Generator[Path, None, None]:
        return (Path(file['text']) for file in self.added_dirs)

    @property
    def include_subdirectories(self) -> bool:
        return self.view.search_subdirectories.active

    def is_selected_added(self):
        selected = Path(self.view.file_picker.selection[0])
        if self.include_subdirectories:
            return any(selected == file or file in selected.parents for file in self.added_as_paths)
        else:
            return any(selected == file for file in self.added_as_paths)

    def set_add_button_state(self):
        if not len(self.view.file_picker.selection) or self.is_selected_added():
            self.view.add_button.disabled = True
        else:
            self.view.add_button.disabled = False

    def search_subdirectories_changed(self):
        if self.include_subdirectories:
            # Merge directories into parents
            for dir1, dir2 in list(combinations(self.added_as_paths, r=2)):
                if dir1 in dir2.parents:
                    self.added_dirs.remove({'text': str(dir2)})
                elif dir2 in dir1.parents:
                    self.added_dirs.remove({'text': str(dir1)})
        self.set_add_button_state()

    def add_path(self, path: Path):
        if self.include_subdirectories:
            # If added directory contains some of already added directories, remove them
            for file in list(self.added_as_paths):
                if path in file.parents:
                    self.added_dirs.remove({'text': str(file)})
        self.added_dirs.append({'text': str(path)})

    def add_selected(self):
        self.view.add_button.disabled = True
        tex = Path(self.view.file_picker.selection[0])
        self.add_path(tex)

    def remove_selected(self):
        selected: int = self.view.picked_list.selected_index
        del self.added_dirs[selected]
        self.view.picked_list.selected_index = None


class SelectableLabel(SelectableWidget, Label):
    COLOR = (0, 0, 0, 1)
    COLOR_SELECTED = (.4, .4, .4, 1)
