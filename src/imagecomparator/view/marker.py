#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Tuple

from kivy.properties import ColorProperty
from kivy.uix.label import Label


class Marker(Label):
    colors = ['2f9650', '16659f', 'd56d41', '6dcaef', 'ef6dc0', 'f1f1ee', 'e93d34', '84e030', '9434b0', 'd2b078',
              '2bb892', '672bca', 'f4e342']
    marker_color = ColorProperty()

    @staticmethod
    def hex_to_rgb(color: str) -> Tuple[float, float, float]:
        return int(color[0:2], 16) / 255, int(color[2:4], 16) / 255, int(color[4:6], 16) / 255

    def __init__(self, group_id: int, **kwargs):
        self.marker_color = self.hex_to_rgb(self.colors[group_id % len(self.colors)])
        super().__init__(**kwargs)
        loop = group_id // len(self.colors)
        self.text = str(loop) if loop else ''
