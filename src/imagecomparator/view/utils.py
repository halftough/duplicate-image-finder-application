#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

def trim(a: float, value_min=None, value_max=None):
    if value_min is not None and a < value_min:
        return value_min
    if value_max is not None and a > value_max:
        return value_max
    return a


def between(a: float, b: float, c: float):
    """Checks if a is between b and c"""
    if b < c:
        return b < a < c
    else:
        return c < a < b