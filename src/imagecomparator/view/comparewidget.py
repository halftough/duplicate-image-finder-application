#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

from enum import Enum
from typing import Dict, Optional, Any, List, Iterable

from kivy.clock import Clock
from kivy.core.window import Window
from kivy.properties import ObjectProperty, NumericProperty, BooleanProperty
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.scrollview import ScrollView
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.widget import Widget

from imagecomparator.model.resultgroup import ResultGroup
from imagecomparator.model.resultgroup import Image as ImageM
from imagecomparator.view.marker import Marker
from imagecomparator.view.sections import CompareMethod, ScalingMethod, Sections


class PickerImage(AnchorLayout, Button):
    index = NumericProperty(None, allownone=True)
    img_texture = ObjectProperty()
    ratio = NumericProperty()
    selected = BooleanProperty(False)
    marked = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.image: ImageM = kwargs['image']
        self.index: int = kwargs['index']
        self.img_texture = Image(source=self.image.path).texture
        self.ratio: float = self.image.ratio
        self._marker_widget: Optional[Marker] = None
        del kwargs['image']
        kwargs['anchor_x'] = 'left'
        kwargs['anchor_y'] = 'top'
        super().__init__(**kwargs)
        self.update_marker()
        self.image.on(ImageM.Action.MARKER_SET, self.update_marker)

    def update_marker(self):
        if self.image.marker_id is not None:
            if self._marker_widget:
                self.remove_widget(self._marker_widget)
            self._marker_widget = Marker(self.image.marker_id)
            self.add_widget(self._marker_widget)


class ImagePickerView(ScrollView):
    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        if self.collide_point(touch.px, touch.py):
            if touch.button == 'scrolldown':
                self.effect_x.value += 20
            elif touch.button == 'scrollup':
                self.effect_x.value -= 20


class CompareImageView(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @property
    def sections(self) -> Sections:
        return self.parent.sections

    # def draw(self, shift: Tuple[float, float] = (0, 0)):
    #     self.sections.draw(shift)

    def on_touch_down(self, touch):
        if self.collide_point(touch.px, touch.py):
            if touch.button == 'scrolldown':
                self.sections.zoom_in()
            elif touch.button == 'scrollup':
                self.sections.zoom_out()
            self.sections.draw()

    def on_touch_move(self, touch):
        if self.collide_point(touch.px, touch.py):
            if touch.button == 'left':
                self.sections.draw((touch.ox - touch.px, touch.oy - touch.py))

    def on_touch_up(self, touch):
        if self.collide_point(touch.px, touch.py):
            if touch.button == 'left':
                self.sections.move((touch.ox - touch.px, touch.oy - touch.py))
                self.sections.draw()


class CompareWidget(BoxLayout):
    marking_mode = BooleanProperty(False)

    class View:
        def __init__(self, ids: Dict[str, Any]):
            self.image_view: CompareImageView = ids['image_view']
            self.comparison_picker: BoxLayout = ids['comparison_picker']
            self.image_picker: BoxLayout = ids['image_picker']
            self.info_button: ToggleButton = ids['info_button']
            self.scale_size: ToggleButton = ids['scale_size']
            self.scale_zoom: ToggleButton = ids['scale_zoom']
            self.method_side_by_side: ToggleButton = ids['method_side_by_side']
            self.method_one_at_time: ToggleButton = ids['method_one_at_time']

    def __init__(self, **kwargs):
        # TODO; Deselecting all images causes a crash
        # TODO; Clear marked
        super(CompareWidget, self).__init__(**kwargs)
        self._view = None
        self._compare_method: CompareMethod = CompareMethod.SIDE_BY_SIDE
        self._scaling_method: ScalingMethod = ScalingMethod.SIZE
        self.group: Optional[ResultGroup] = None
        self.image_widgets: List[PickerImage] = []
        self.sections = Sections(self)
        self.marking_mode = False
        self.ctrl = False
        self._keyboard = Window.request_keyboard(lambda: None, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_key_up)

    @property
    def screen(self):
        return self.parent.parent if self.parent else None

    @property
    def view(self):
        if self._view is None and self.ids:
            self._view = self.View(self.ids)
            self._view.image_view.bind(size=self._update)
            for button in self.comparison_button_map.keys():
                button.bind(on_release=self.compare_method_button_clicked)
            for button in self.scaling_method_button_map.keys():
                button.bind(on_release=self.scaling_method_button_clicked)
        return self._view

    @property
    def selected(self) -> List[int]:
        return [i.index for i in self.image_widgets if i.selected]

    @property
    def selected_images(self) -> List[ImageM]:
        return [i.image for i in self.image_widgets if i.selected]

    @property
    def marked(self) -> List[int]:
        return [i.index for i in self.image_widgets if i.marked]

    @property
    def comparison_button_map(self):
        return {
            self.view.method_side_by_side: CompareMethod.SIDE_BY_SIDE,
            self.view.method_one_at_time: CompareMethod.ONE_AT_TIME
        }

    @staticmethod
    def _set_grouped_method_value(map: Dict[Button, Enum], value: Enum):
        for button, method in map.items():
            if method == value:
                button.state = 'down'
            else:
                button.state = 'normal'

    @property
    def scaling_method_button_map(self):
        return {
            self.view.scale_size: ScalingMethod.SIZE,
            self.view.scale_zoom: ScalingMethod.ZOOM
        }

    @property
    def compare_method(self):
        return self._compare_method

    @compare_method.setter
    def compare_method(self, value: CompareMethod):
        self._compare_method = value
        self._set_grouped_method_value(self.comparison_button_map, value)
        self.fill_missing_selects()

    @property
    def scaling_method(self) -> ScalingMethod:
        return self._scaling_method

    @scaling_method.setter
    def scaling_method(self, value: ScalingMethod):
        self._scaling_method = value
        self._set_grouped_method_value(self.scaling_method_button_map, value)
        self.sections.scale_update()

    def compare_method_button_clicked(self, button):
        self.compare_method = self.comparison_button_map[button]

    def scaling_method_button_clicked(self, button):
        self.scaling_method = self.scaling_method_button_map[button]

    def toggle_compare_method(self):
        methods = list(self.comparison_button_map.values())
        current_index = methods.index(self.compare_method)
        new_index = current_index + 1 if current_index + 1 < len(methods) else 0
        self.compare_method = methods[new_index]

    def toggle_scale(self):
        methods = list(self.scaling_method_button_map.values())
        current_index = methods.index(self.scaling_method)
        new_index = current_index + 1 if current_index + 1 < len(methods) else 0
        self.scaling_method = methods[new_index]

    def toggle_info(self):
        self.view.info_button.state = 'down' if self.view.info_button.state == 'normal' else 'normal'
        self.sections.draw()

    def update_sections(self):
        # TODO; replace calculate_sections in some cases
        # TODO; Consider removing it entirely and use sections.calculate_sections
        self.sections.calculate_sections()

    def _update(self, *args):
        self.update_sections()

    def set_match(self, group: Optional[ResultGroup]):
        if self.group is not None:
            self.group.remove_handlers(ResultGroup.Action.IMAGE_ADDED)
            for image in self.group.images:
                image.remove_handlers()
        self.group = group
        if group is not None:
            self.group.on(ResultGroup.Action.IMAGE_ADDED, self.new_image_found)
        self._fill_image_picker()

    def new_image_found(self, image):
        Clock.schedule_once(lambda _: self._add_image(image))

    def update_image_picker_width(self):
        w = 0
        for child in self.view.image_picker.children:
            w += child.width
        w += self.view.image_picker.spacing * (len(self.view.image_picker.children) - 1)
        self.view.image_picker.width = w

    def _add_image(self, image):
        index = len(self.image_widgets)
        selected = index < self.compare_method.min_select()
        image_widget = PickerImage(image=image, index=index, selected=selected)
        self.image_widgets.append(image_widget)
        image_widget.bind(on_release=self._image_clicked)
        self.view.image_picker.add_widget(image_widget)
        self.update_image_picker_width()

    def _fill_image_picker(self):
        self.view.image_picker.clear_widgets()
        self.image_widgets.clear()
        if self.group is None:
            self.view.image_view.canvas.clear()
            self.sections.clear()
        else:
            for image in self.group:
                self._add_image(image)
            self.sections.calculate_sections()

    def fill_missing_selects(self):
        # TODO; Redraw Images picker
        if not self.screen.selected_group:
            return
        # Add first unselected images until minimum number is reached
        while len(self.selected) < self.compare_method.min_select():
            img = next(filter(lambda a: not a.selected, self.image_widgets), None)
            if img is not None:
                img.selected = True
        # Remove last selected images while number is over maximum
        while self.compare_method.max_select() is not None and len(self.selected) > self.compare_method.max_select():
            img = next(filter(lambda a: a.selected, self.image_widgets[::-1]), None)
            if img is not None:
                img.selected = False
        self.update_sections()

    def _image_clicked(self, image: PickerImage):
        if self.marking_mode != self.ctrl:
            image.marked = not image.marked
        else:
            if self.compare_method == CompareMethod.SIDE_BY_SIDE:
                image.selected = not image.selected
                self.sections.update_sections()
            elif self.compare_method == CompareMethod.ONE_AT_TIME:
                old_zoom = self.sections.leading_section.zoom
                for image_widget in self.image_widgets:
                    image_widget.selected = False
                image.selected = True
                self.sections.leading_section.change_current(image.image)
                self.sections.leading_section.zoom = old_zoom
                self.sections.draw()

    used_shortcuts = []

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'lctrl':
            self.ctrl = True

    def _on_key_up(self, keyboard, keycode):
        if keycode[1] == 'lctrl':
            self.ctrl = False

    def _remove_from_image_picker(self, images: Iterable[Image]):
        removed_count = 0
        for child in list(self.image_widgets):
            if child.image in images:
                self.view.image_picker.remove_widget(child)
                self.image_widgets.remove(child)
                removed_count += 1
            child.index -= removed_count

    def mark_unselected(self):
        for image in self.image_widgets:
            if not image.selected:
                image.marked = True

    def remove_marked(self):
        if self.group:
            images = [self.group[i] for i in self.marked]
            group = self.group
            group.remove(images)
            if group is self.group:
                self._remove_from_image_picker(images)
                self.fill_missing_selects()
            for image in images:
                image.delete()
