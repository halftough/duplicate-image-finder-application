#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Dict, Any, Optional

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.progressbar import ProgressBar
from kivy.uix.widget import Widget

from kivyextras.uix.shortcutscreen import ShortcutScreen
from imagecomparator.model.resultgroup import ResultGroup
from imagecomparator.model.searchengine import SearchEngine
from imagecomparator.view.comparewidget import CompareWidget
from kivyextras.uix.selectablerecycleview import SelectableRecycleView, SelectableWidget
from imagecomparator.view.filepickerscreen import FilePickerScreen
from imagecomparator.view.sections import CompareMethod, ScalingMethod


class ResultsView(SelectableRecycleView):
    pass


class MatchImage(Image):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class MatchGroupView(SelectableWidget):
    COLOR = (0.1, 0.1, 0.1, 1)
    COLOR_SELECTED = (.5, .5, 1, .3)

    class View:
        def __init__(self, ids: Dict):
            self.layout: BoxLayout = ids['layout']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.view = self.View(self.ids)

    def refresh_view_attrs(self, rv, index, data: Dict[str, ResultGroup]):
        self.index = index
        self.view.layout.clear_widgets()
        for image in data['match']:
            self.view.layout.add_widget(MatchImage(source=image.path))
        self.view.layout.add_widget(Widget())
        return super(MatchGroupView, self).refresh_view_attrs(rv, index, data)


class MainScreen(ShortcutScreen):
    screen_name = 'main_screen'

    class View:
        def __init__(self, ids: Dict[str, Any]):
            self.progress_bar: ProgressBar = ids['progress_bar']
            self.results: ResultsView = ids['results']
            self.compare_widget: CompareWidget = ids['compare_widget']

    def __init__(self, **kwargs):
        super().__init__(name=self.screen_name, **kwargs)
        self.view = self.View(self.ids)
        self.search_engine: SearchEngine = SearchEngine()
        self.selected_result_index: Optional[int] = None
        self.selected_group: Optional[ResultGroup] = None
        self.view.results.bind(selected_index=self.on_group_selected)
        self.view.results.bind(data=self._on_data_changed)
        self.used_shortcuts = {
            'toggle_compare_method': self.view.compare_widget.toggle_compare_method,
            'compare_side_by_side': self._set_compare_side_by_side,
            'compare_one_at_time': self._set_compare_one_at_time,
            'toggle_zoom': self.view.compare_widget.sections.zoom_toggle,
            'zoom_fit': self.view.compare_widget.sections.zoom_fit,
            'zoom_100': self.view.compare_widget.sections.zoom_100,
            'toggle_scaling': self.view.compare_widget.toggle_scale,
            'scaling_size': self._set_scaling_size,
            'scaling_zoom': self._set_scaling_zoom,
            'toggle_information': self.view.compare_widget.toggle_info,
            'remove_marked': self.view.compare_widget.remove_marked
        }
        self.update_combinations()

    def _set_compare_side_by_side(self):
        self.view.compare_widget.compare_method = CompareMethod.SIDE_BY_SIDE

    def _set_compare_one_at_time(self):
        self.view.compare_widget.compare_method = CompareMethod.ONE_AT_TIME

    def _set_scaling_size(self):
        self.view.compare_widget.scaling_method = ScalingMethod.SIZE

    def _set_scaling_zoom(self):
        self.view.compare_widget.scaling_method = ScalingMethod.ZOOM

    def set_selected_group(self, index: Optional[int], group: Optional[ResultGroup] = None):
        if self.selected_result_index is not None:
            self.selected_group.off(ResultGroup.Action.IMAGE_REMOVED, self._on_images_removed)
        self.selected_result_index = index
        self.selected_group = group
        self.view.compare_widget.set_match(self.selected_group)
        if self.selected_group:
            self.selected_group.on(ResultGroup.Action.IMAGE_REMOVED, self._on_images_removed)

    def on_enter(self, *args):
        app: App = App.get_running_app()
        filepicker = app.root.get_screen(FilePickerScreen.screen_name)
        self.search_engine.on(SearchEngine.Action.PROGRESS_CHANGED, self.update_progress_bar)
        self.search_engine.on(SearchEngine.Action.RESULTS_CHANGED, self.update_results)
        self.search_engine.start_search(filepicker.picked_dirs_list, filepicker.include_subdirectories)
        self.view.results.data = self.search_engine.matches

    def on_group_selected(self, instance: ResultsView, index: Optional[int]):
        if index is not None:
            self.set_selected_group(index, instance.data[index]['match'])

    def _on_images_removed(self, group, images):
        # update view
        self.view.results.data = self.search_engine.results_data

    def _on_data_changed(self, rv: ResultsView, data):
        if rv.selected_index is not None and rv.selected_index < len(data):
            group = data[rv.selected_index]['match']
            if group is not self.view.compare_widget.group:
                self.set_selected_group(rv.selected_index, group)
        else:
            self.set_selected_group(None)

    def update_progress_bar(self, value: float):
        self.view.progress_bar.value = value

    def update_results(self, results: List[Dict]):
        self.view.results.data = results

    def on_config_change(self):
        super().on_config_change()
        self.view.compare_widget.sections.draw()

    def on_exit(self):
        self.search_engine.stop_search()
