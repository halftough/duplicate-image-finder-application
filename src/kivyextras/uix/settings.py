#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional

from kivy.core.window import Window
from kivy.metrics import dp
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.settings import SettingString, SettingSpacer, SettingItem
from kivy.uix.widget import Widget

from kivyextras.utils import WatchList


# TODO likely to remove
class SettingShortcutLayout(BoxLayout):
    def __init__(self, **kwargs):
        if 'orientation' not in kwargs:
            kwargs['orientation'] = 'vertical'
        if 'spacing' not in kwargs:
            kwargs['spacing'] = '5dp'
        super(SettingShortcutLayout, self).__init__(**kwargs)


class SettingShortcut(SettingString):
    def __init__(self, **kwargs):
        super(SettingShortcut, self).__init__(**kwargs)
        self.keys_label: Optional[Label] = None
        self._keyboard = Window.request_keyboard(lambda: None, self)
        self.pressed_keys = []
        self.showed_keys = WatchList()
        self.showed_keys.on(changed=self._update_keys_label)
        self.clear_btn = Button(size_hint=(None, None), text="clear")
        self.clear_btn.bind(on_release=self._clear_clicked)
        self.add_widget(self.clear_btn)

    def on_touch_down(self, touch):
        if self.clear_btn.collide_point(touch.x, touch.y):
            super(SettingItem, self).on_touch_down(touch)
        else:
            super(SettingShortcut, self).on_touch_down(touch)

    def _create_popup(self, instance):
        # create popup layout
        self.showed_keys.set_list(self._keys_from_str(self.value))
        content = SettingShortcutLayout()
        popup_width = min(0.95 * Window.width, dp(500))
        self.popup = popup = Popup(
            title=self.title, content=content, size_hint=(None, None),
            size=(popup_width, '250dp'))

        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_key_up)

        instructions = Label(text='Insert key or combination.', font_size='24sp', size_hint_y=None, height='42sp')
        self.keys_label = keys_label = Label(text='', font_size='22sp', size_hint_y=None, height='42sp')

        # construct the content, widget are used as a spacer
        content.add_widget(Widget())
        content.add_widget(instructions)
        content.add_widget(keys_label)
        content.add_widget(Widget())
        content.add_widget(SettingSpacer())

        # 2 buttons are created for accept or cancel the current value
        btnlayout = BoxLayout(size_hint_y=None, height='50dp', spacing='5dp')
        self.ok_btn = btn = Button(text='Ok')
        btn.bind(on_release=self._validate)
        btnlayout.add_widget(btn)

        btn = Button(text='Cancel')

        btn.bind(on_release=self._dismiss)
        btnlayout.add_widget(btn)
        content.add_widget(btnlayout)

        # all done, open the popup !
        self._update_keys_label(self.showed_keys)
        popup.open()

    def str_value(self, keys):
        s = ''
        keys.sort(key=self._key_cmp)
        for key in keys:
            if s:
                s += ' + '
            s += key.capitalize()
        return s

    @staticmethod
    def _keys_from_str(value: str):
        return value.lower().split(' + ')

    def _update_keys_label(self, keys):
        if self.keys_label and self.ok_btn:
            self.keys_label.text = self.str_value(keys)
            self.ok_btn.disabled = not keys

    def _dismiss(self, *args):
        super()._dismiss(*args)
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard.unbind(on_key_up=self._on_key_up)

    def _validate(self, instance):
        self._dismiss()
        self.value = self.str_value(self.showed_keys)

    def _clear_clicked(self, *_):
        self.value = ''
        self.showed_keys.set_list([])

    mod_keys = ['ctrl', 'super', 'alt', 'shift']

    key_map = {
        'rctrl': 'ctrl',
        'lctrl': 'ctrl',
        'alt-gr': 'alt',
        'rshift': 'shift'
    }

    def _key_cmp(self, key):
        return self.mod_keys.index(key) if key in self.mod_keys else len(self.mod_keys)

    def _key(self, keycode):
        key = keycode[1]
        return self.key_map.get(key, key)

    def _on_keyboard_down(self, _, keycode, text, modifiers):
        key = self._key(keycode)
        if key not in self.mod_keys and any(k not in self.mod_keys for k in self.pressed_keys):
            return
        if key not in self.pressed_keys:
            self.pressed_keys.append(key)
        self.showed_keys.set_list(self.pressed_keys)

    def _on_key_up(self, _, keycode):
        key = self._key(keycode)
        if key not in self.showed_keys:
            return
        self.pressed_keys.remove(key)
        if key in self.mod_keys and all(k in self.mod_keys for k in self.showed_keys):
            self.showed_keys.remove(key)
