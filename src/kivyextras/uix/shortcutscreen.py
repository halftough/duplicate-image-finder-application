#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Callable, Dict

from kivy import Config
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen

from kivyextras.uix.settings import SettingShortcut


class ShortcutScreen(Screen):
    used_shortcuts: Dict[str, Callable] = {}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._keyboard = Window.request_keyboard(lambda: None, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.combinations = {}
        self.update_combinations()

    def update_combinations(self):
        config = Config.get_configparser('app')
        self.combinations.clear()
        for shortcut, function in self.used_shortcuts.items():
            combination = config.get('shortcuts', shortcut).lower()
            if combination:
                self.combinations[combination] = function

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        keys = []
        for mod in modifiers:
            if mod in SettingShortcut.mod_keys:
                keys.append(mod)
        if keycode[1] not in modifiers:
            keys.append(keycode[1])

        combination = ' + '.join(keys)
        function = self.combinations.get(combination, None)
        if function is not None:
            function()

    def on_config_change(self):
        self.update_combinations()
