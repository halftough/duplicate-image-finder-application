#     DuplicateFinder – application looking for similar images in a set
#
#     Copyright (C) 2020-2021 Marcin Gurtowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

from typing import Iterable


def watch_change(func):
    def inner(self: WatchList, *args, **kwargs):
        before = self._list.copy()
        ret = func(self, *args, **kwargs)
        if before != self._list:
            self._run_on_change_watchers()
        return ret

    return inner


class WatchList:
    def __init__(self, seq=()):
        self._list = list(seq)
        self._append_watchers = []
        self._changed_watchers = []

    def on(self, **kwargs):
        for action, func in kwargs.items():
            watchers = getattr(self, f'_{action}_watchers', None)
            if watchers is None or not isinstance(watchers, list):
                raise TypeError(f'on got an unexpected keyword argument \'{action}\'')
            watchers.append(func)

    def _run_on_change_watchers(self):
        for func in self._changed_watchers:
            func(self)

    @watch_change
    def append(self, obj) -> None:
        self._list.append(obj)

    @watch_change
    def clear(self) -> None:
        self._list.clear()

    def count(self, value):
        return self._list.count(value)

    @watch_change
    def extend(self, iterable: Iterable) -> None:
        self._list.extend(iterable)

    def index(self, *args, **kwargs):
        return self._list.index(*args, **kwargs)

    @watch_change
    def insert(self, index: int, obj) -> None:
        self._list.insert(index, obj)

    @watch_change
    def pop(self, index: int = -1):
        self._list.pop(index)

    @watch_change
    def remove(self, value) -> None:
        self._list.remove(value)

    @watch_change
    def reverse(self) -> None:
        self._list.reverse()

    @watch_change
    def set_list(self, l: list):
        self._list = l.copy()

    @watch_change
    def sort(self, *args, **kwargs) -> None:
        self._list.sort(*args, **kwargs)

    def __contains__(self, item):
        return self._list.__contains__(item)

    def __delattr__(self, item):
        return self._list.__delattr__(item)

    def __delitem__(self, key):
        return self._list.__delitem__(key)

    def __eq__(self, other):
        return self._list.__eq__(other)

    def __getitem__(self, item):
        return self._list.__getitem__(item)

    def __gt__(self, other):
        return self._list.__gt__(other)

    def __iter__(self):
        return self._list.__iter__()

    def __le__(self, other):
        return self._list.__le__(other)

    def __len__(self):
        return self._list.__len__()

    def __lt__(self, other):
        return self._list.__lt__(other)

    def __ne__(self, other):
        return self._list.__ne__(other)

    def __repr__(self):
        return self._list.__repr__()

    def __setitem__(self, key, value):
        return self._list.__setitem__(key, value)

    def __str__(self):
        return self._list.__str__()
